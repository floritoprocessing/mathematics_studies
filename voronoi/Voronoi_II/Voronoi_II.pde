Voronoi[] v;
PGraphics pg;

void setup() {
  size(800,600,P3D);
  pg = createGraphics(width,height/2,P3D);
  pg.beginDraw();
  pg.background(255);
  pg.endDraw();
  background(255);
  
  v = new Voronoi[150];
  boolean randomPoints = false;
  if (randomPoints) {
    for (int i=0;i<v.length;i++) {
      v[i] = new Voronoi(random(width),random(height/2));
    }
  } else {
    
    PImage img = loadImage("cells1.jpg");
    //PImage img = loadImage("Kraak_Smaak1-800x300.jpg");
    //PImage img = loadImage("MVI_7667.jpg");
    pg.beginDraw();
    pg.image(img,0,0,width,height/2);
    pg.endDraw();
    for (int i=0;i<v.length;i++) {
      float bright = 0;
      float x=0,y=0;
      while (bright<220) {
        x = random(img.width);
        y = random(img.height);
        bright = brightness(img.get((int)x,(int)y));
      }
      v[i] = new Voronoi(x*width/img.width,y*height/2/img.height);
    }
//    PImage img = loadImage("Kraak_Smaak1-800x300.jpg");
//    img.loadPixels();
//    pg.beginDraw();
//    pg.image(img,0,0);
//    pg.endDraw();
//    Pix[] arr = new Pix[img.pixels.length];
//    int x=0,y=0;
//    for (int i=0;i<img.pixels.length;i++) {
//      arr[i] = new Pix(x,y,(int)brightness(img.pixels[i]));
//      x++;
//      if (x==img.width) {
//        x=0;
//        y++;
//      }
//    }
//    Arrays.sort(arr);
//    for (int i=0;i<v.length;i++) {
//      v[i] = new Voronoi(arr[i].x, arr[i].y);
//    }
  }
}

void draw() {
  stroke(0);
  pg.beginDraw();
  for (int l=0;l<50;l++) 
  for (int i=0;i<v.length;i++) {
    v[i].increaseArea();
    v[i].increaseDegree();
    v[i].draw(pg);
  }
  pg.endDraw();
  image(pg,0,height/2);
}
