class Pix implements Comparable {
  
  int x, y, dark;
  Pix(int x, int y, int dark) {
    this.x=x;
    this.y=y;
    this.dark=dark;
  }
  
  public int compareTo(Object o) {
    Pix p = (Pix)o;
    if (p.dark<dark) return 1;
    else if (p.dark>dark) return -1;
    return 0;
  }
  
}
