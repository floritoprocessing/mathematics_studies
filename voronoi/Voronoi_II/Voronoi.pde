class Voronoi {
  
  int col;
  float x, y;
  float a=0, r=0, circum=0;
  float deg = random(360);
  int dir = Math.random()<0.5?-1:1;
  float inc = random(0.2,1);
  
  boolean[] taken = new boolean[360];
  
  Voronoi(float x, float y) {
    this.x=x;
    this.y=y;
    col = color(random(128,255),random(128,255),random(128,255));
  }
  
  void increaseArea() {
    increaseArea(inc);
  }
  
  void increaseArea(float inc) {
    a += inc;
    r = sqrt(a/PI);
    circum = TWO_PI*r;
  }
    
  void increaseDegree() {
    deg += dir*360.0/circum*0.5;
    while (deg<0) deg+=360;
    while (deg>=360) deg-=360;
  }
  
  void draw(PGraphics pg) {
    if (!taken[(int)deg]) {
      float rd = radians(deg);
      float dx = x + r * cos(rd);
      float dy = y + r * sin(rd);
      //TODO: check along line for other color
      float len = dist(x,y,dx,dy);
      for (float p=0;p<len;p++) {
        // point on line
        float px = x + p * cos(rd);
        float py = y + p * sin(rd);
        int bgCol = get((int)px,(int)py);
        if (px<0||py<0||px>width||py>height/2) {
          taken[(int)deg] = true;
        }
        if (bgCol!=col && bgCol!=color(255)) {
          taken[(int)deg] = true;
        }
        if (taken[(int)deg]) {
          
          
          pg.stroke(255);
          pg.point(px,py);
          //stroke(0);
          //point(px,py);
          
          boolean drawLines = false;
          if (drawLines) {
            float l = 50;
            float ax = l * cos(rd+HALF_PI);//+HALF_PI);
            float ay = l * sin(rd+HALF_PI);//+HALF_PI);
            //pg.stroke(red(col),green(col),blue(col),15);
            pg.stroke(255,5);
            pg.line(px-ax,py-ay,px+ax,py+ay);
          }
          p = len+1;
        }
      }
      if (!taken[(int)deg]) {
        stroke(col);
        line(x,y,dx,dy);
      }
    }
    //fill(255);
    //ellipse(x,y,3,3);
  }
  
}
