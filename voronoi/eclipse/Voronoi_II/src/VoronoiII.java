import java.util.Vector;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class VoronoiII extends PApplet {
	
	float x0 = 20;
	float y0 = 20;
	float x1, y1;
	Vector<Circle> circles = new Vector<Circle>();
	Circle addCircle = null;
	PImage image = null;
	
	public void settings() {
		size(1024,768,P3D);
	}
	public void setup() {
		
		//image = loadImage("landscape.jpg");
		
		Circle.STEPS = 360;
		
		textFont(createFont("Verdana",10));
		textAlign(LEFT, TOP);
		smooth();
	}
	
	public void mousePressed() {
		addCircle = new Circle(mouseX,mouseY);
		addCircle.areaInc = 60;//random(15,30);
		if (image!=null)
			addCircle.color = image.get(mouseX, mouseY);//
		else
			addCircle.color = color(random(128,255),random(128,255),random(128,255));
	}
	
	/*public void mouseDragged() {
		addCircle = new Circle(mouseX,mouseY);
	}*/
	
	
	public void draw() {
		
		if (addCircle!=null) {
			circles.add(addCircle);
			addCircle=null;
		}
		
		if (circles.size()<6) {
			background(255,0,0);
			return;
		}
		long ti = millis();
		
		if (image!=null) {
			background(image);
		} else {
			background(255);
		}
		rectMode(CORNER);
		noStroke(); fill(255,192);
		rect(0,0,width,height);
		
		
		for (Circle circle:circles) {
			if (circle.totallyDead) {
				stroke(0);
				fill(circle.color);
			} else {
				noStroke();
				fill(circle.color);
			}
			circle.draw(this);
			rectMode(CENTER);
			noStroke();
			fill(0);
			rect(circle.x, circle.y, 3, 3);
		}
		long drawTime = millis()-ti;
		
		for (Circle circle:circles) {
			if (!circle.totallyDead) {
				circle.increaseArea();
				circle.areaInc += 1;
			}
		}
		
		
		ti = millis();
		for (Circle circle:circles) {
			Circle.calcOverlap(circle, this, 10);
		}
		
		int circleAmount = circles.size();
		for (int i=0;i<circleAmount-1;i++) {
			for (int j=i+1;j<circleAmount;j++) {
				Circle.calcOverlap(circles.get(i), circles.get(j));
			}
		}
		long calcTime = millis()-ti;
		text("calcTime="+calcTime+"ms",10,10);
		text("drawTime="+drawTime+"ms",10,25);
		//println("calc time = "+dt);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"--present","VoronoiII"});
	}
}
