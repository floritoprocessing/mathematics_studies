import processing.core.PApplet;
import processing.core.PConstants;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Circle {
	
	public static int STEPS = 360;
	
	float x, y;
	//float x0=0, y0=0, x1=0, y1=0;
	float area = 0.01f, radius=0, areaInc = 1;
	float[] cos = new float[STEPS], sin = new float[STEPS];
	
	float[] circleX = new float[STEPS], circleY = new float[STEPS];
	boolean[] alive = new boolean[STEPS];
	boolean totallyDead = false;

	public int color = 0xffff0000;
	
	public Circle(float x, float y) {
		
		this.x=x;
		this.y=y;
		/*this.x0=x0;
		this.x1=x1;
		this.y0=y0;
		this.y1=y1;*/
		for (int i=0;i<STEPS;i++) {
			double rd = Math.PI*2*(double)i/(double)STEPS;
			cos[i] = (float)Math.cos(rd);
			sin[i] = (float)Math.sin(rd);
			alive[i] = true;
		}
		calcCircle();
	}
	
	public void increaseArea() {
		if (!totallyDead) {
			area += areaInc;
			calcCircle();
		}
	}
	
	private void calcCircle() {
		radius = (float)Math.sqrt(area/(2*Math.PI));
		for (int i=0;i<STEPS;i++) {
			if (alive[i]) {
				circleX[i] = x + radius * cos[i];
				circleY[i] = y + radius * sin[i];
			}
		}
	}
	
	public void draw(PApplet pa) {
		pa.beginShape();
		for (int i=0;i<STEPS;i++) {
			pa.vertex(circleX[i], circleY[i]);
		}
		pa.endShape(PConstants.CLOSE);
	}

	
	public static void calcOverlap(Circle c, PApplet pa, float inset) {
		if (!c.totallyDead) {
			
			boolean any1Alive = false;
			
			for (int i=0;i<STEPS;i++) {
				if (c.alive[i]) {
					if (c.circleX[i]<inset||c.circleY[i]<inset||c.circleX[i]>=pa.width-inset||c.circleY[i]>=pa.height-inset) {
						c.alive[i] = false;
					}
				}
				any1Alive = any1Alive | c.alive[i];
			}
			if (!any1Alive) {
				c.totallyDead = true;
			}
		}
	}
	
	/**
	 * @param circle
	 * @param circle2
	 */
	public static void calcOverlap(Circle c0, Circle c1) {
		
		Circle circle0 = c0;
		Circle circle1 = c1;
		
		for (int pass=0;pass<2;pass++) {
		
			if (pass==1) {
				circle0 = c1;
				circle1 = c0;
			}
			
			if (!circle0.totallyDead) {
				float radius = circle1.radius;
				float radiusSq = radius*radius;
				float x1 = circle1.x;
				float y1 = circle1.y;
				
				boolean any1Alive = false;
			
				for (int i=0;i<STEPS;i++) {
					if (circle0.alive[i]) {
						float x0 = circle0.circleX[i];
						float y0 = circle0.circleY[i];
						float dx = x1-x0;
						float dy = y1-y0;
						float dSq = dx*dx+dy*dy;
						if (dSq<radiusSq) {
							circle0.alive[i] = false;
						}
					}
					any1Alive = any1Alive | circle0.alive[i];
				}
				if (!any1Alive) {
					circle0.totallyDead = true;
				}
			}
		
		}
		
	}
}
