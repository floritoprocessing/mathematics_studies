package voronoi1;

import delaunay.Pnt;

public class MovingPoint {
	
	private Pnt position;
	private Pnt movement;

	public MovingPoint(Pnt position, Pnt movement) {
		this.position = position;
		position.dimCheck(movement);
		this.movement = movement;
	}
	
	public Pnt getPosition() {
		return position;
	}
	
	public void move(double time, Pnt target, int width, int height) {
		
		Pnt toTarget = target.subtract(position);
		Pnt acceleration = new Pnt(0,0);
		
		double distance = toTarget.magnitude();
		if (distance>5) {
			double strength = 1000/(distance*distance);
			acceleration = toTarget.multiply(strength/distance);			
		}
		
		movement.translate( acceleration.multiply(time) );
		position.translate( movement.multiply(time) );
		
		
		double cenX = width/2.0;
		double cenY = height/2.0;
		double max = Math.sqrt(cenX*cenX+cenY*cenY);
		double dToCen = position.subtract(new Pnt(cenX,cenY)).magnitude();
		if (dToCen>max) {
			movement = movement.multiply(0.8);
		}
		
		//movement = movement.multiply(0.999);
		
	}
	
	
}
