package voronoi1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

import delaunay.Pnt;
import delaunay.Triangle;
import delaunay.Triangulation;
import processing.core.PApplet;

public class Voronoi_I extends PApplet {

	private static final long serialVersionUID = 2306971913883072052L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"voronoi1.Voronoi_I"});
	}
	
	private Triangulation dt;                   // Delaunay triangulation
	private Triangle initialTriangle;           // Initial triangle
	private static int initialSize = 100000;     // Size of initial triangle
	
	private Pnt sun;
	private ArrayList<MovingPoint> points;
	
	public void settings() {
		size(800,600,P3D);
	}
	public void setup() {
		
		smooth();
		initialTriangle = new Triangle(
                new Pnt(-initialSize, -initialSize),
                new Pnt( initialSize, -initialSize),
                new Pnt(           0,  initialSize));
        dt = new Triangulation(initialTriangle);
        
        points = new ArrayList<MovingPoint>();
        sun = new Pnt(width/2,height/2);
        
        for (int i=0;i<100;i++) {
        	addSite(random(0.3f*width,0.7f*width),random(0.3f*height,0.7f*height));
        }
	}
	
	private void addSite(double x, double y) {
		Pnt pos = new Pnt(new double[] {x,y});
		Pnt mov = new Pnt(new double[] {random(-1,1),random(-1,1)});
		MovingPoint p = new MovingPoint(pos,mov);
		points.add(p);
	}
	
	public void draw() {
		background(255);
		
		dt = new Triangulation(initialTriangle);
		for (MovingPoint p:points) {
			dt.delaunayPlace(p.getPosition());
		}
		drawAllVoronoi(true);
		
		for (MovingPoint p:points) {
			p.move(0.2,sun,width,height);
		}
	}
	
	public void drawAllVoronoi (boolean withSites) {
        // Keep track of sites done; no drawing for initial triangles sites
        HashSet<Pnt> done = new HashSet<Pnt>(initialTriangle);
        for (Triangle triangle : dt) {
           for (Pnt site: triangle) {
                if (done.contains(site)) continue;                
                done.add(site);
                
                List<Triangle> list = dt.surroundingTriangles(site, triangle);
                Pnt[] vertices = new Pnt[list.size()];
                int i = 0;
                for (Triangle tri: list) vertices[i++] = tri.getCircumcenter();
                
                //stroke(random(255),random(255),random(255));
                stroke(0,0,0,64);
                draw(vertices);//, 0xff000000);
                
                if (withSites) draw(site);
            }
        }
    }

	public void draw (Pnt point) {
        int r = 4;//pointRadius;
        float x = (float) point.coord(0);
        float y = (float) point.coord(1);
        //g.fillOval(x-r, y-r, r+r, r+r);
        ellipseMode(CENTER);
        stroke(0);
        ellipse(x,y,r,r);
    }
	
	public void draw (Pnt[] polygon) {
        float[] x = new float[polygon.length];
        float[] y = new float[polygon.length];
        for (int i = 0; i < polygon.length; i++) {
            x[i] = (float) polygon[i].coord(0);
            y[i] = (float) polygon[i].coord(1);
        }
        beginShape();
        for (int i=0;i<x.length;i++) {
        	if (x[i]>=0&&x[i]<width&&y[i]>=0&&y[i]<height)
        		vertex(x[i],y[i]);
        }
        endShape(CLOSE);

    }
	
}
