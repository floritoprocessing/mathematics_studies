import processing.video.*;

boolean saveMovie = false;
//MovieMaker mm;  // Declare MovieMaker object

VoronoiCircle[] v = new VoronoiCircle[15];
boolean drawingModeLines = true;

void setup() {
  size(800, 600);
  smooth();
  background(255);
  noStroke();
  textFont(createFont("Verdana", 12));
  //fill(255,0,0);
  initVoronoi();
  
  //if (saveMovie) {
  //  int dd = day();    // Values from 1 - 31
  //  int mo = month();  // Values from 1 - 12
  //  int yy = year();   // 2003, 2004, 2005, etc.
  //  int s = second();  // Values from 0 - 59
  //  int m = minute();  // Values from 0 - 59
  //  int h = hour();    // Values from 0 - 23
    
  //  String d = nf(yy,4)+nf(mo,2)+nf(dd,2)+"_"+nf(h,2)+nf(m,2)+nf(s,2);
  //  String name = "voronoi_I_"+d+".mov";
  //  println("Saving movie to "+name);
  //  mm = new MovieMaker(this, width, height, name,
  //                       25, MovieMaker.JPEG, MovieMaker.LOSSLESS);
  //}
}

void initVoronoi() {
  background(255);
  for (int i=0;i<v.length;i++) {
    v[i] = new VoronoiCircle(random(width*0.05, width*0.95), random(height*0.05, height*0.95), 0);
  }
}

void mousePressed() {
  initVoronoi();
}

void keyPressed() {
  if (key==' ') {
    drawingModeLines = !drawingModeLines;
  } else if (key=='+') {
    v = new VoronoiCircle[v.length+1];
    initVoronoi();
  } else if (key=='-' && v.length>1) {
    v = new VoronoiCircle[v.length-1];
    initVoronoi();
  }
}

void draw() {

  //background(255);
  for (int i=0;i<v.length;i++) {
    v[i].draw(this, drawingModeLines);
    v[i].increaseArea(200);
    v[i].generatePoints();
  }

  for (int i=0;i<v.length-1;i++) {
    for (int j=i+1;j<v.length;j++) {

      VoronoiCircle v0 = v[i];
      VoronoiCircle v1 = v[j];

      if (v0.voronoiAlive && v1.voronoiAlive) {

        for (int c0=0;c0<VoronoiCircle.NR_OF_POINTS;c0++) {
          int i0 = c0, i1 = (c0+1)%VoronoiCircle.NR_OF_POINTS;
          PVector p0 = v0.c[i0];
          PVector p1 = v0.c[i1];
          for (int c1=0;c1<VoronoiCircle.NR_OF_POINTS;c1++) {
            int i2 = c1, i3 = (c1+1)%VoronoiCircle.NR_OF_POINTS;
            PVector p2 = v1.c[i2];
            PVector p3 = v1.c[i3];
            Object intersect = lineLineIntersection(p0, p1, p2, p3);
            if (intersect.getClass().equals(PVector.class)) {
              PVector ip = (PVector)intersect;
              if (ip.z!=0) {
                v0.alive[i0] = false;
                v0.alive[i1] = false;
                v1.alive[i2] = false;
                v1.alive[i3] = false;
              }
            }
          }
        }
      }
    }
  }

  stroke(0);
  fill(255);
  rect(5,5,250,65);
  fill(0);
  text("VoronoiTest ("+v.length+" voronois)", 10, 20);
  text("Click to initialize", 10, 35);
  text("[SPACE] to change drawing mode", 10, 50);
  text("[+]/[-] to change number of voronois", 10, 65);
  
  //if (saveMovie) mm.addFrame();  // Add window's pixels to movie
}

//void dispose() {
//  if (saveMovie) {
//    println("Saved a movie");
//    mm.finish();  // Finish the movie if space bar is pressed!
//  }
//}
