static class VoronoiCircle {
  
  static int NR_OF_POINTS = 36;
  float x, y;
  float area;
  PVector[] c = new PVector[NR_OF_POINTS];
  boolean[] alive = new boolean[NR_OF_POINTS];
  boolean voronoiAlive = true;
  
  float r = (float)(Math.random()*128);
  float g = (float)(Math.random()*128);
  float b = (float)(Math.random()*128);
  
  VoronoiCircle(float x, float y, float area) {
    this.x = x;
    this.y = y;
    this.area = area;
    for (int i=0;i<NR_OF_POINTS;i++) {
      c[i] = new PVector();
      alive[i] = true;
    }
    generatePoints();
  }
  
  void increaseArea(float inc) {
    area += inc;
  }
  
  void generatePoints() {
    float r = sqrt(area/PI);
    for (int i=0;i<NR_OF_POINTS;i++) {
      float rd = (float)i / (float)NR_OF_POINTS * TWO_PI;
      if (alive[i]) {
        c[i] = new PVector( x + r * cos(rd), y + r * sin(rd), 0 );
      }
    }
    
    voronoiAlive = false;
    for (int i=0;i<NR_OF_POINTS;i++) {
      voronoiAlive = voronoiAlive | alive[i];
    }
  }
  
  void draw(PApplet pa, boolean drawingModeLines) {
    //pa.fill(r,g,b,32);
    if (drawingModeLines) {
      pa.stroke(r,g,b,32); pa.noFill();
    } else {
      pa.noStroke(); pa.fill(r,g,b,32);
    }
    pa.beginShape(POLYGON);
    for (int i=0;i<NR_OF_POINTS;i++) {
      pa.vertex(c[i].x, c[i].y);
    }
    pa.endShape(CLOSE);
  }
  
}
