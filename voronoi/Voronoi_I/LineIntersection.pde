// line1: P1..P2
// line2: P3..P4

Object lineLineIntersection(PVector P1, PVector P2, PVector P3, PVector P4) {

  float x1 = P1.x, y1 = P1.y;
  float x2 = P2.x, y2 = P2.y;
  float x3 = P3.x, y3 = P3.y;
  float x4 = P4.x, y4 = P4.y;

  float d = ((y4-y3)*(x2-x1)-(x4-x3)*(y2-y1));
  float na = ((x4-x3)*(y1-y3)-(y4-y3)*(x1-x3));
  float ua = na / d;
  float nb = ((x2-x1)*(y1-y3)-(y2-y1)*(x1-x3));
  float ub = nb / d;
  
  if (d==0) {
    return "parallel";
  }
  else if (d==0 && na==0 && nb==0) {
    return "coincident";
  }
  else {
    
    float x = x1 + ua*(x2-x1);
    float y = y1 + ua*(y2-y1);
    PVector p = new PVector(x,y,0);
    
    boolean segmentIntersection = false;
    if (ua>0&&ua<1&&ub>0&&ub<1) {
      segmentIntersection = true;
    }
    
    p.z = segmentIntersection ? 1:0;
    
    return p;
  }
  
  //return null;
  
}



PVector p0, p1, p2, p3;
void newLine() {
  p0 = new PVector(random(width),random(height));
  p1 = new PVector(random(width),random(height));
  p2 = new PVector(random(width),random(height));
  p3 = new PVector(random(width),random(height));
  
  Object result = lineLineIntersection(p0,p1,p2,p3);
  if (result.getClass().equals(String.class)) {
    println(result);
  } else if (result.getClass().equals(PVector.class)) {
    PVector v = (PVector)result;
    println("Intersection: "+v.x+"/"+v.y+". "+(v.z==1?"Segment":"Line")+" intersection.");
  }
  
}
