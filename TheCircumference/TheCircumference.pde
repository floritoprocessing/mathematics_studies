import java.text.*;

float radius;
int n = 5; // sides in the polygon


void setup() {
  size(1200, 800,P3D); 
  noFill();
  radius = height/2 * 0.9;
}

void draw() {
  background(255);
  translate(height/2, height/2);
  resetText(height/2, -height/2+20);
  drawCircle();
  drawTriangles();
  drawTriangleDetail();
}

void keyPressed() {
  if (keyCode==UP || keyCode==RIGHT) {
    if (n<10) n++;
    else if (n<100) n+=10;
    else if (n<1000) n+=100;
    else if (n<10000) n+=1000;
    else n+=10000;
  }
  if ((keyCode==DOWN || keyCode==LEFT) && n>3) {
    if (n>10000) n-=10000;
    else if (n>1000) n-=1000;
    else if (n>100) n-=100;
    else if (n>10) n-=10;
    else n--;
  }
}


void drawCircle() {
  stroke(0);
  strokeWeight(3);
  noFill();
  ellipseMode(RADIUS);
  ellipse(0, 0, radius, radius);
}

void drawTriangles() {
  fill(0);
  screenPrintLn("Division of the circle with r="+nf(radius, 1, 1)+" into triangles:");
  screenPrintLn("n = "+n);
  stroke(128, 0, 0);
  strokeWeight(1);
  for (int i=0; i<n; i++) {
    float rd = map(i, 0, n, 0, TWO_PI)-HALF_PI;
    float rd1 = map(i+1, 0, n, 0, TWO_PI)-HALF_PI;
    float cx = cos(rd)*radius;
    float cy = sin(rd)*radius;
    line(0, 0, cx, cy);
    float cx1 = cos(rd1)*radius;
    float cy1 = sin(rd1)*radius;
    line(cx, cy, cx1, cy1);
    line(cx1, cy1, 0, 0);
  }
}

void drawTriangleDetail() {
  strokeWeight(2);

  // triangle;
  float rd0 = map(0, 0, n, 0, TWO_PI)-HALF_PI;
  float rd1 = map(1, 0, n, 0, TWO_PI)-HALF_PI;
  float cx0 = cos(rd0)*radius;
  float cy0 = sin(rd0)*radius;
  float cx1 = cos(rd1)*radius;
  float cy1 = sin(rd1)*radius;
  stroke(255, 0, 0);
  fill(255, 0, 0);
  line(0, 0, cx0, cy0);
  textAlign(LEFT, CENTER);
  text("c", cx0/2+5, cy0/2);
  line(cx1, cy1, 0, 0);

  //screenPrintLn("r (radius) = "+nf(radius,1,1));
  screenPrintLn("c = r");
  float d = 2*radius;
  fill(0);
  screenPrintLn("diameter d = 2*r = "+nf(d, 1, 1));
  screenPrintLn();
  float c = radius;
  float phi = TWO_PI/n;

  float fac = 0.2;
  float fix0 = fac*cx0, fiy0 = fac*cy0;
  float fix1 = fac*cx1, fiy1 = fac*cy1;
  fill(255, 0, 0);
  textAlign(CENTER, BOTTOM);
  text("φ", (fix0+fix1)/2, (fiy0+fiy1)/2);
  noFill();
  arc(0, 0, radius*fac, radius*fac, -HALF_PI, -HALF_PI+phi);
  //line(fix0,fiy0

  float theta = (PI-phi)/2;
  float thx0 = (1-fac)*cx0, thy0 = (1-fac)*cy0;
  float thx1 = (1-fac)*cx0+fac*cx1, thy1 = (1-fac)*cy0+fac*cy1;
  textAlign(LEFT, TOP);
  text("θ", (thx0+thx1)/2, (thy0+thy1)/2);
  noFill();
  //arc(cx0,cy0, fac*radius, fac*radius, HALF_PI-theta, HALF_PI);
  line(thx0, thy0, thx1, thy1);
  fill(255, 0, 0);
  screenPrintLn("φ (phi) = 360°/n = 360°/"+n+" = "+nf(degrees(phi), 1, 1)+"°");
  screenPrintLn("θ (theta) = (180°-φ)/2 = "+nf(degrees(theta), 1, 1)+"°");


  // isoscles triangle: 
  // baseline = 2 * side * cos (θ)
  fill(0, 128, 0);
  screenPrintLn();
  screenPrintLn("Calc height a of isoscles triangle c, c, baseline");
  float a = c * sin(theta);
  screenPrintLn("a = c * sin(θ) = c * sin("+nf(degrees(theta), 1, 1)+") = "+nf(a, 1, 2));

  stroke(0, 0, 255);
  fill(0, 0, 255);
  line(cx0, cy0, (cx0+cx1)/2, (cy0+cy1)/2);
  textAlign(CENTER, TOP);
  text("b", (cx0*0.75+cx1*0.25), (cy0*0.75+cy1*0.25)+5);
  float b = sqrt(c*c-a*a);
  println("b from vecs: "+dist(cx0, cy0, cx1, cy1)*2);
  screenPrintLn();
  screenPrintLn("Derive b from pythagoras a*a + b*b = c*c");
  screenPrintLn("b = sqrt(c*c-a*a) = "+nf(b, 1, 1));

  float m = n*2*b;
  fill(128, 0, 0);
  screenPrintLn();
  screenPrintLn("Circumference m = n*2*b = "+n+"*2*"+nf(b, 1, 1)+" = "+nf(m, 1, 1) );

  screenPrintLn();
  float pi = m/d;
  fill(0);
  screenPrintLn("Ratio of circumference m to diameter d = "+pi);




  screenPrintLn();

  // FORMULA SIMPLIFICATION

  //float result = n*2*sqrt(radius*radius-radius*radius*sin(theta)*sin(theta));
  //float resultCircum = n*2*sqrt(radius*radius*(1-sin((PI-TWO_PI/n)/2)*sin((PI-TWO_PI/n)/2)));
  //screenPrintLn(resultCircum+"");

  //float ratioCircumToDiam = n*2*sqrt(radius*radius*(1-sin((PI-TWO_PI/n)/2)*sin((PI-TWO_PI/n)/2)))/(2*radius);
  //sqrtfloat ratioCircumToDiam = n*sqrt(radius*radius*(1-sin((PI-TWO_PI/n)/2)*sin((PI-TWO_PI/n)/2)))/radius;
  //float ratioCircumToDiam = n*sqrt(1-sin((PI-TWO_PI/n)/2)*sin((PI-TWO_PI/n)/2));
  //float ratioCircumToDiam = n*sqrt(1- sin(HALF_PI-PI/n) * sin(HALF_PI-PI/n) );
  /* sin(a-b) = sin(a)*cos(b)-sin(b)*cos(a)
   sin(HALF_PI-PI/n) -> sin(HALF_PI)*cos(PI/n)-SIN(PI/n)*cos(HALF_PI)
   since sin(HALF_PI)=1 and cos(HALF_PI)=0
   = cos(PI/n)
   */
  //float ratioCircumToDiam = n*sqrt(1- cos(PI/n) * cos(PI/n) );
  // since 1-cos(a)*cos(a) = sin(a)*sin(a):
  //float ratioCircumToDiam = n*sqrt(sin(PI/n) * sin(PI/n) );
  double ratioCircumToDiam = n*Math.sin(Math.PI/n);
  NumberFormat nf = NumberFormat.getInstance();
  nf.setMinimumFractionDigits(15);

  screenPrintLn("Simplifying the formula results in:");
  screenPrintLn();
  screenPrintLn("PI = "+nf.format(Math.PI));
  screenPrintLn("pi = n*sin(180°/n) = "+nf.format(ratioCircumToDiam));

  double distToPi = Math.abs(Math.PI-ratioCircumToDiam);
  double accuracy = 1-distToPi/PI;
  screenPrintLn("Accuracy to PI is "+nf.format(accuracy*100)+"%");
  //String finalFormula = "n*2*sqrt(r*r-r*r*sin(θ)*sin(θ))";

  // half line down
  float cHalfX = (cx0+cx1)/2;
  float cHalfY = (cy0+cy1)/2;
  stroke(0, 192, 0);
  fill(0, 192, 0);
  line(0, 0, cHalfX, cHalfY);
  textAlign(CENTER, RIGHT);
  text("a", cHalfX/2-5, cHalfY/2);
}
