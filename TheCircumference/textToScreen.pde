String printBuffer = "";
float textX;
float textY = 20;

void resetText(float x, float y) {
  textX = x;
  textY = y;
}
void screenPrintLn() {
  screenPrintLn("");
}
void screenPrintLn(String txt) {
  doScreenPrint(txt, true);
}
void screenPrint(String txt) {
  doScreenPrint(txt, false);
}

void doScreenPrint(String txt, boolean nextLine) {
  printBuffer += txt;
  if (nextLine) {
    textAlign(LEFT, BASELINE);
    text(printBuffer, textX, textY);
    textY += textAscent()+textDescent();
    printBuffer = "";
  }
}
