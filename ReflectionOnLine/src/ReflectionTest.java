import florito.geom.Line;
import florito.geom.MultiLine;
import florito.geom.Point;
import processing.core.PApplet;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class ReflectionTest extends PApplet {
	
	private static final long serialVersionUID = -3419142878772924369L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"ReflectionTest"});
	}
	
	Point X = new Point(100,100);
	Line line = new Line(50,150,150,200);
	MultiLine ml = new MultiLine();
	
	Line mirror = new Line(450, 50, 300, 300);
	boolean overP1, overP2;
	
	public void settings() {
		size(800,600,P3D);
	}
	public void setup() {
		
		smooth();
		
		createMultiLine();
	}

	/**
	 * 
	 */
	private void createMultiLine() {
		ml = new MultiLine();
		Point p1 = new Point(200,200);
		Point p2 = null;
		for (int i=0;i<5;i++) {
			p2 = new Point(p1.x + random(-100,100), p1.y + random(-100,100));
			Line l = new Line(p1,p2);
			ml.addLine(l);
			p1 = new Point(p2);
		}
	}
	
	public void keyPressed() {
		createMultiLine();
	}
	
	public void mouseDragged() {
		if (overP1) {
			mirror.setP1(new Point(mouseX,mouseY));
		}
		if (overP2) {
			mirror.setP2(new Point(mouseX,mouseY));
		}
	}
	
	public void draw() {
		background(255);
		
		/*
		 * Draw X
		 */
		fill(0);
		X.draw(this, 5);
		
		/*
		 * Draw line
		 */
		stroke(0);
		line.drawLine(this, false);
		
		/*
		 * Draw multiLine
		 */
		stroke(0);
		ml.draw(this);
		
		stroke(0,0,255);
		strokeWeight(3);
		mirror.drawLine(this, false);
		strokeWeight(1);
		mirror.drawLine(this, true);
		fill(0,0,255);
		mirror.drawPoints(this, 5);
		
		/*
		 * Check distance mouse - mirror points
		 */
		Point mp1 = mirror.getP1();
		if (dist(mp1.x, mp1.y, mouseX, mouseY)<5) {
			overP1 = true;
		} else {
			overP1 = false;
		}
		Point mp2 = mirror.getP2();
		if (dist(mp2.x, mp2.y, mouseX, mouseY)<5) {
			overP2 = true;
		} else {
			overP2 = false;
		}
		if (overP1) {
			noFill();
			stroke(0);
			mp1.draw(this, 8);
		}
		if (overP2) {
			noFill();
			stroke(0);
			mp2.draw(this, 8);
		}
		
		/*
		 * Reflect X -> Xm
		 */
		
		Point Xm = Line.reflect(X, mirror);
		noFill();
		stroke(0);
		Xm.draw(this, 5);
		
		/*
		 * Reflect line -> lineM
		 */
		Line lineM = Line.reflect(line, mirror);
		stroke(0);
		lineM.drawLine(this, false);
		
		/*
		 * Reflect multiline
		 */
		MultiLine mlm = MultiLine.reflect(ml, mirror);
		stroke(0);
		mlm.draw(this);
	}
}
