import florito.geom.Line;
import florito.geom.Point;
import processing.core.PApplet;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class GeomTest extends PApplet {
	
	private static final long serialVersionUID = 9079678155938353024L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"GeomTest"});
	}
	
	Line line1 = new Line(120,320,380,50);
	Line line2 = new Line(100,100,500,350);
	Line line3 = new Line(100,150,500,400);
	Point intersection12, intersection23;
	Point perpPoint = new Point(400,200);
	Line perpLine;
	
	public void settings() {
		size(640,480,P3D);
	}
	public void setup() {
		
		ellipseMode(RADIUS);
		
		intersection12 = Line.intersection(line1, line2);
		System.out.println("intersection12: "+intersection12);
		
		intersection23 = Line.intersection(line2, line3);
		System.out.println("intersection23: "+intersection23);
		
		perpLine = Line.perpendicular(line1, perpPoint);
	}
	
	public void draw() {
		background(255);
		
		/*
		 * Red: line 1
		 */
		
		fill(255,0,0);
		line1.drawPoints(this,3);
		stroke(255,0,0);
		line1.drawHorizontal(this,0,width,3);
		
		/*
		 * Green: line 2
		 */
		
		fill(0,255,0);
		line2.drawPoints(this,3);
		stroke(0,255,0);
		line2.drawHorizontal(this,0,width,3);
		
		/*
		 * Blue: line 3, parallel to line 2
		 */
		fill(0,0,255);
		line3.drawPoints(this,3);
		stroke(0,0,255);
		line3.drawHorizontal(this,0,width,3);
		
		/*
		 * Intersection of line 1 and line 2
		 */
		fill(0);
		noStroke();
		ellipse(intersection12.x,intersection12.y,3,3);
		
		/*
		 * Brown: line perpendicular to line1 through brown point perpPoint
		 */
		fill(128,128,0);
		ellipse(perpPoint.x, perpPoint.y, 3, 3);
		stroke(128,128,0);
		perpLine.drawHorizontal(this,0,width,3);
	}
}
