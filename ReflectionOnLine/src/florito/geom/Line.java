package florito.geom;
import processing.core.PApplet;

/**
 * 
 */

/**
 * Definition of a line: Ax + By = C. <br>
 * Based on the <a href="http://www.topcoder.com/tc?module=Static&d1=tutorials&d2=geometry2#reflection">algorithm tutorial</a> by By lbackstrom. 
 * @author Marcus
 *
 */
public class Line {
	
	/**
	 * Returns the intersection point between line m and line n.<br>
	 * Returns <i>null</i> if the lines are parallel
	 * @param m
	 * @param n
	 * @return
	 */
	public static Point intersection(Line m, Line n) {
		float A1 = m.A;
		float B1 = m.B;
		float C1 = m.C;
		float A2 = n.A;
		float B2 = n.B;
		float C2 = n.C;
		
		float det = A1*B2-A2*B1;
		if (det==0) {
			return null; // lines are parallel
		} else {
			float x = (B2*C1-B1*C2)/det;
			float y = (A1*C2-A2*C1)/det;
			return new Point(x,y);
		}
	}
	
	/**
	 * Creates a new Line perpendicular to the given line through the given point
	 * @param line
	 * @param point
	 * @return
	 */
	public static Line perpendicular(Line line, Point point) {
		float A = line.A;
		float B = line.B;
		float x = point.x;
		float y = point.y;
		// perpendicular:
		// -B*x + A*y = D
		float D = -B*x + A*y;
		return new Line(-B, A, D);
	}
	
	/**
	 * Reflects the given point on a line
	 * @param point
	 * @param reflectionLine
	 * @return
	 */
	public static Point reflect(Point point, Line reflectionLine) {
		Line perpendicular = perpendicular(reflectionLine, point);
		Point intersectionPointY = intersection(reflectionLine, perpendicular);
		Point mirrorPoint = Point.sub(intersectionPointY,Point.sub(point,intersectionPointY));
		return mirrorPoint;
	}
	
	/**
	 * Reflects the given line on the reflectionLine
	 * @param line
	 * @param reflectionLine
	 * @return
	 */
	public static Line reflect(Line line, Line reflectionLine) {
		Point p1 = reflect(line.p1, reflectionLine);
		Point p2 = reflect(line.p2, reflectionLine);
		Line newLine = new Line(p1, p2);
		return newLine;
	}
	
	float A, B, C;
	Point p1 = null;
	Point p2 = null;
	
	/**
	 * Create a line defined by two points
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public Line(float x1, float y1, float x2, float y2) {
		p1 = new Point(x1,y1);
		p2 = new Point(x2,y2);
		deriveABCFromPoints();
	}
	
	/**
	 * Create a line defined by two points
	 * @param p1
	 * @param p2
	 */
	public Line(Point p1, Point p2) {
		this.p1 = new Point(p1);
		this.p2 = new Point(p2);
		deriveABCFromPoints();
	}
	
	public void setP1(Point p) {
		p1 = new Point(p);
		deriveABCFromPoints();
	}
	
	public void setP2(Point p) {
		p2 = new Point(p);
		deriveABCFromPoints();
	}

	/**
	 * Derive the constants A, B, C fromt the two given points
	 */
	private void deriveABCFromPoints() {
		A = p2.y-p1.y;
		B = p1.x-p2.x;
		C = A*p1.x + B*p1.y;
	}
	
	/**
	 * Creates a line defined as A*x + B*y = C;
	 * @param A
	 * @param B
	 * @param C
	 */
	public Line(float A, float B, float C) {
		this.A = A;
		this.B = B;
		this.C = C;
	}
	
	public float getX(float y) {
		//A*x + B*y = C
		//A*x = C - B*y
		float x = (C - B*y)/A;
		return x;
	}
	
	public float getY(float x) {
		//A*x + B*y = C
		//B*y = C - A*x
		float y = (C - A*x)/B;
		return y;
	}

	/**
	 * Draws the two points x1/y1 and x2/y2
	 * @param pa
	 * @param radius
	 */
	public void drawPoints(PApplet pa, float radius) {
		if (p1!=null)
			p1.draw(pa, radius);
		if (p2!=null)
			p2.draw(pa, radius);
	}

	/**
	 * @param pa
	 * @param xMin
	 * @param xMax
	 * @param xStep
	 */
	public void drawHorizontal(PApplet pa, float xMin, float xMax, float xStep) {
		for (float x=xMin;x<=xMax;x+=xStep) {
			float y = getY(x);
			pa.point(x, y);
		}
	}
	
	/**
	 * 
	 * @param pa
	 * @param endless
	 */
	public void drawLine(PApplet pa, boolean endless) {
		if (!endless) {
			pa.line(p1.x, p1.y, p2.x, p2.y);
		} else {
			float x1 = 0;
			float y1 = getY(x1);
			float x2 = pa.width;
			float y2 = getY(x2);
			pa.line(x1, y1, x2, y2);
		}
	}
	
	/**
	 * @param pa
	 * @param yMin
	 * @param yMax
	 * @param yStep
	 */
	public void drawVertical(PApplet pa, float yMin, float yMax, float yStep) {
		for (float y=yMin;y<=yMax;y+=yStep) {
			float x = getX(y);
			pa.point(x, y);
		}
	}
	
	public Point getP1() {
		return new Point(p1);
	}
	
	public Point getP2() {
		return new Point(p2);
	}

	@Override
	public String toString() {
		return "Line["+A+"*x + "+B+"*y = "+C+"]";
	}
	
	
}
