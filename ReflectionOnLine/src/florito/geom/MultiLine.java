/**
 * 
 */
package florito.geom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import processing.core.PApplet;

/**
 * @author Marcus
 *
 */
public class MultiLine {
	
	public static MultiLine reflect(MultiLine multiLine, Line reflectionLine) {
		ArrayList<Line> newLines = new ArrayList<Line>();
		for (Line l:multiLine.lines) {
			newLines.add(Line.reflect(l, reflectionLine));
		}
		return new MultiLine(newLines);
	}
	
	ArrayList<Line> lines = new ArrayList<Line>();
	
	public MultiLine() {
	}
	
	public MultiLine(Line... lines) {
		this.lines.addAll(Arrays.asList(lines));
	}
	
	public MultiLine(Collection<Line> lines) {
		this.lines.addAll(lines);
	}
	
	public void addLine(Line line) {
		lines.add(line);
	}
	
	public void draw(PApplet pa) {
		for (Line l:lines) {
			l.drawLine(pa, false);
		}
	}
}
