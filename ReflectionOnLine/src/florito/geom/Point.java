package florito.geom;

import processing.core.PApplet;
/**
 * 
 */

/**
 * Point class
 * @author Marcus
 *
 */
public class Point {
	
	public float x, y;

	public Point() {
	}
	
	public Point(float x, float y) {
		this.x=x;
		this.y=y;
	}
	
	public Point(Point p) {
		this.x = p.x;
		this.y = p.y;
	}

	@Override
	public String toString() {
		return "Point["+x+", "+y+"]";
	}

	/**
	 * @param p1
	 * @param p2
	 * @return
	 */
	public static Point sub(Point p1, Point p2) {
		return new Point(p1.x-p2.x, p1.y-p2.y);
	}
	
	public void draw(PApplet pa, float radius) {
		pa.ellipseMode(PApplet.RADIUS);
		pa.ellipse(x,y,radius,radius);
	}
	
}
