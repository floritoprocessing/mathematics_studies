/*
Add two successive Fibonacci numbers such as 1, 2, 3, 5, 8, 13... and you'll get the next. 
The corresponding ratios 1/1, 1/2, 2/3, 3/5, 5/8, 8/13, 13/21, 21/34 ... 
converge to the harmonic proportion: 1/2 (square root of 5) - 1/2 = 0.618034...
*/

int amount = 10;

void setup() {
  size(800,600,JAVA2D);
  smooth();
  
  PFont font = loadFont("ArialMT-12.vlw");
  textFont(font);
}

void draw() {
  background(0);
  
  int[] fib = new int[amount+1];
  
  fib[0] = 1;
  fib[1] = 2;
  
  for (int i=2;i<fib.length;i++) {
    fib[i] = fib[i-2] + fib[i-1];
  }
  
  float segWidth = (float)width/amount;
  
  for (int i=0;i<amount;i++) {
    float x0 = i * segWidth;
    float x1 = (i+1) * segWidth;
    float xc = (x0 + x1)/2;
    
    fill(255);
    textAlign(CENTER);
    text(fib[i]+"/"+fib[i+1],xc,20);
    
    float fibratio = (float)fib[i]/(float)fib[i+1];
        
    float convergence = 0.5f * sqrt(5) - 0.5f;
    float y = ratioToY(convergence);
    stroke(0,0,255);
    line(0,y,width,y);
    
    stroke(255,0,0);
    line(x0,ratioToY(fibratio),x1,ratioToY(fibratio));
    
  }
  
  noLoop();
}

float ratioToY(float fibratio) {
  return height - height * fibratio;
}
